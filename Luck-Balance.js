'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', function () {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the luckBalance function below.
function luckBalance(k, contests) {
    let important = contests.filter(ar => ar[1] === 1).length;

    let sumAll = contests.reduce((a, b) => a + b[0], 0);

    let sorted = contests.sort((a, b) => a[0] - b[0])

    let win = important - k >= 0 ? important - k : 0

    let min = 0

    for (let i = 0; i < sorted.length; i++) {
        if (win === 0) break;
        if (sorted[i][1] === 0) continue;
        min += sorted[i][0];
        win--
    }

    return sumAll - (2 * min);
}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const nk = readLine().split(' ');

    const n = parseInt(nk[0], 10);

    const k = parseInt(nk[1], 10);

    let contests = Array(n);

    for (let i = 0; i < n; i++) {
        contests[i] = readLine().split(' ').map(contestsTemp => parseInt(contestsTemp, 10));
    }

    const result = luckBalance(k, contests);

    ws.write(result + '\n');

    ws.end();
}