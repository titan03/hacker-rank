'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', function () {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the substrCount function below.
function substrCount(n, s) {
    let result = 0
    for (let i = 0; i < s.length; i++) {
        result += CounterSpecialSubStr(s, i)
    }
    return result
}

function CounterSpecialSubStr(s, i) {
    let count = 0
    let length = 0
    const start = 1

    while (s[start] === s[i] && length < s.length) {
        count++;
        i++;
        length++;
    }


    while (length > 0) {
        if (s[i] !== s[start] || i === s.length) {
            return count
        }
        length--;
        i++;
    }

    return count
}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const n = parseInt(readLine(), 10);

    const s = readLine();

    const result = substrCount(n, s);

    ws.write(result + '\n');

    ws.end();
}